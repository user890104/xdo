import express from 'express';
import * as dotenv from 'dotenv';
import bodyParser from 'body-parser';
import XdoTool from 'xdotool';

dotenv.config();

const allowedCmds = [
    'XF86AudioPrev',
    'XF86AudioPlay',
    'XF86AudioNext',
    'XF86AudioMute',
    'XF86AudioLowerVolume',
    'XF86AudioRaiseVolume',
];

const {
    XdoToolAsync,
    XdoToolBindings,
} = XdoTool;

const xdo = new XdoToolAsync(new XdoToolBindings());
const app = express();

app.use(express.static('public'));
app.use(bodyParser.urlencoded({
    extended: false,
}));

app.post('/', async (req, res) => {
    const cmd = req.body?.cmd;

    if (cmd && allowedCmds.indexOf(cmd) > -1) {
        await xdo.sendKeysequence((await xdo.getFocusedWindow()).toString(), cmd, 0);
        res.status(204).send('');
    }
    else {
        res.status(422).send('');
    }
});

app.listen(process.env.PORT || 3000);
